import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp(functions.config().firebase);

import * as hello from './hello';

export const helloWorld = hello.helloWorld;